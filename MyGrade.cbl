       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MYGRADE.
       AUTHOR. SIRAPATSON.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT MYGRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT GRADE-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION.
       FILE SECTION.
       FD MYGRADE-FILE.
       01 MYGRADE-DETAILS.
           88 END-OF-MYGRADE-FILE VALUE HIGH-VALUES.
           05 COURSE-CODE       PIC X(6).
           05 COURSE-NAME       PIC X(50).
           05 CREDIT            PIC 9(1).
           05 GRADE             PIC X(2).
       FD GRADE-FILE.
       01  GRADE-DETAIL.
           05  AVG-NAME       PIC X(18).
           05  AVG-GRADE      PIC 9(1)V9(3).

       WORKING-STORAGE SECTION.
       01  SUM-AVG-GRADE  PIC 9(1)V9(3).
       01  NUM-GRADE      PIC 9(1)V9(2).
       01  CREDIT-GRADE   PIC 9(2)V9(2).
       01  SUM-CREDIT     PIC 9(3).
       01  SUM-GRADE      PIC 9(3)V9(3).

       01  SUM-AVG-SCI-GRADE       PIC 9(1)V9(3).
       01  SCI-CREDIT              PIC 9(3).
       01  SUM-SCI-GRADE-CREDIT    PIC 9(3)V9(3).

       01  SUM-AVG-CS-GRADE        PIC 9(1)V9(3).
       01  CS-CREDIT               PIC 9(3).
       01  SUM-CS-GRADE-CREDIT     PIC 9(3)V9(3).
       
       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT MYGRADE-FILE
           OPEN OUTPUT GRADE-FILE

           PERFORM UNTIL END-OF-MYGRADE-FILE
              READ MYGRADE-FILE 
                 AT END SET END-OF-MYGRADE-FILE TO TRUE 
              END-READ

              IF NOT END-OF-MYGRADE-FILE THEN
                 PERFORM 001-PROCESS1 THRU 001-EXIT
              END-IF 
           END-PERFORM

           PERFORM 002-SUM-AGV THRU 002-EXIT
           
           CLOSE MYGRADE-FILE
           CLOSE GRADE-FILE

           GOBACK 
           .

       001-PROCESS1.
           EVALUATE TRUE 
              WHEN GRADE = "A"    MOVE 4    TO NUM-GRADE 
              WHEN GRADE = "B+"   MOVE 3.5  TO NUM-GRADE 
              WHEN GRADE = "B"    MOVE 3    TO NUM-GRADE 
              WHEN GRADE = "C+"   MOVE 2.5  TO NUM-GRADE 
              WHEN GRADE = "C"    MOVE 2    TO NUM-GRADE 
              WHEN GRADE = "D+"   MOVE 1.5  TO NUM-GRADE 
              WHEN GRADE = "D"    MOVE 1    TO NUM-GRADE 
              WHEN OTHER MOVE 0  TO NUM-GRADE
           END-EVALUATE

           COMPUTE CREDIT-GRADE = CREDIT * NUM-GRADE 
           COMPUTE SUM-CREDIT = SUM-CREDIT + CREDIT 
           COMPUTE SUM-GRADE = SUM-GRADE + CREDIT-GRADE  
           COMPUTE SUM-AVG-GRADE = SUM-GRADE / SUM-CREDIT

           IF COURSE-CODE(1:1) IS EQUAL TO "3" THEN
              COMPUTE SCI-CREDIT = SCI-CREDIT + CREDIT 
              COMPUTE SUM-SCI-GRADE-CREDIT 
                 = (CREDIT * NUM-GRADE) + SUM-SCI-GRADE-CREDIT
              COMPUTE SUM-AVG-SCI-GRADE = 
                 SUM-SCI-GRADE-CREDIT / SCI-CREDIT 
           END-IF

           IF COURSE-CODE(1:2) IS EQUAL TO "31" THEN
              COMPUTE CS-CREDIT = CS-CREDIT + CREDIT 
              COMPUTE SUM-CS-GRADE-CREDIT 
                 = (CREDIT * NUM-GRADE) + SUM-CS-GRADE-CREDIT
              COMPUTE SUM-AVG-CS-GRADE = 
                 SUM-CS-GRADE-CREDIT / CS-CREDIT 
           END-IF

           .

       001-EXIT.
           EXIT.

       002-SUM-AGV. 
           DISPLAY "======================================"
           DISPLAY "AVG-GRADE : " SUM-AVG-GRADE
           DISPLAY "======================================"
           DISPLAY "AVG-SCI-GRADE : " SUM-AVG-SCI-GRADE
           DISPLAY "======================================"
           DISPLAY "AVG-CS-GRADE : " SUM-AVG-CS-GRADE 
           DISPLAY "======================================"

           MOVE "AVG-GRADE : " TO AVG-NAME
           MOVE SUM-AVG-GRADE  TO AVG-GRADE IN GRADE-DETAIL
           WRITE GRADE-DETAIL

           MOVE "AVG-SCI-GRADE : " TO AVG-NAME
           MOVE SUM-AVG-SCI-GRADE TO AVG-GRADE IN GRADE-DETAIL 
           WRITE GRADE-DETAIL

           MOVE "AVG-CS-GRADE : " TO AVG-NAME
           MOVE SUM-AVG-CS-GRADE TO AVG-GRADE IN GRADE-DETAIL 
           WRITE GRADE-DETAIL
           .

       002-EXIT.
           EXIT.
